from unittest import TestCase

from storage_classes import BoxPlacement, Box, Vector3D


class TestBoxPlacement(TestCase):
    box = Box(size=Vector3D(2, 2, 2))
    
    def test_away(self):
        p1 = BoxPlacement(box=self.box, pos=Vector3D(0, 0, 0))
        p2 = BoxPlacement(box=self.box, pos=Vector3D(20, 0, 0))
        p1.validate_no_intersection(p2)
        p2.validate_no_intersection(p1)
    
    def test_adjacent(self):
        p1 = BoxPlacement(box=self.box, pos=Vector3D(0, 0, 0))
        p2 = BoxPlacement(box=self.box, pos=Vector3D(2, 0, 0))
        p1.validate_no_intersection(p2)
        p2.validate_no_intersection(p1)
    
    def test_identical(self):
        p1 = BoxPlacement(box=self.box, pos=Vector3D(0, 0, 0))
        p2 = BoxPlacement(box=self.box, pos=Vector3D(0, 0, 0))

        with self.assertRaises(ValueError):
            p1.validate_no_intersection(p2)
        with self.assertRaises(ValueError):
            p2.validate_no_intersection(p1)
    
    def test_intersection(self):
        p1 = BoxPlacement(box=self.box, pos=Vector3D(0, 0, 0))
        p2 = BoxPlacement(box=self.box, pos=Vector3D(1, 1, 1))

        with self.assertRaises(ValueError):
            p1.validate_no_intersection(p2)
        with self.assertRaises(ValueError):
            p2.validate_no_intersection(p1)
