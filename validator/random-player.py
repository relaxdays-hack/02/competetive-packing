import logging as log
from typing import List
from random import randint

from flask import Flask, request

from storage_classes import Vector3D, Box, BoxPlacement
import config

app = Flask(__name__)

log.basicConfig(level=config.log_level)
log.getLogger('werkzeug').setLevel(log.WARNING)

container_size: Vector3D
boxes_own: List[Box]
boxes_opponent: List[Box]
placed_boxes: List[BoxPlacement]


@app.route("/ping")
def ping():
    return "I'm good to go!"


@app.post("/new-game")
def new_game():
    global container_size
    global boxes_own
    global boxes_opponent
    global placed_boxes
    
    assert request.is_json
    log.info(f'received new game state')
    
    container_size = Vector3D.from_size_dict(request.json['container'])
    boxes_own = list(map(Box.from_dict, request.json['parcels']))
    boxes_opponent = list(map(Box.from_dict, request.json['opponent_parcels']))
    placed_boxes = []
    log.debug(f'container: {container_size}')
    log.debug(f'own boxes: {boxes_own}')
    log.debug(f'opponents boxes: {boxes_opponent}')
    return ''


@app.post("/opponent-parcel")
def opponent_parcel():
    try:
        box_placement = BoxPlacement(
                box=Box.from_dict(request.json['parcel']),
                pos=Vector3D.from_position_dict(request.json['position'])
        )
        log.info(f'received opponents placement: {box_placement}')
        placed_boxes.append(box_placement)
        boxes_opponent.remove(box_placement.box)
    except BaseException as e:
        log.warning('an error occurred during processing of the opponents move', exc_info=e)
    
    return ''


@app.get("/next-parcel")
def next_parcel():
    box = boxes_own.pop(0)
    box_placement = BoxPlacement(
            box=box,
            pos=Vector3D(
                    x=randint(0, container_size.x - box.size.x),
                    y=randint(0, container_size.y - box.size.y),
                    z=randint(0, container_size.z - box.size.z),
            )
    )
    placed_boxes.append(box_placement)
    log.info(f'submitting own placement: {box_placement}')
    
    return {
        'parcel': box_placement.box.as_dict(),
        'position': box_placement.pos.as_position_dict(),
    }
