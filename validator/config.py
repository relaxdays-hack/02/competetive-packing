import logging


container_size = {
    'x': 10,
    'y': 10,
    'z': 10,
}

boxes = {
    'dimensions_min': 1,
    'dimensions_max': 3,
    'volume_min': 2,
    # multiplier for total volume during box generation, should be >1
    'volume_provisioning': 1.1
}

# URLs of the player servers, don't add a / at the end!
# player1_url = 'http://random-player1:5000'
# player2_url = 'http://random-player2:5000'
player1_url = 'http://main:8080'
player2_url = 'http://main2:8080'

timout_default = 5

log_level = logging.DEBUG
