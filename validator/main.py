import logging as log
import math
from typing import List
from random import randint

import config
from game_state import GameState
from storage_classes import Vector3D, Box


def generate_single_box() -> Box:
    volume_min = config.boxes['volume_min']
    dimensions_min = config.boxes['dimensions_min']
    dimensions_max = config.boxes['dimensions_max']
    
    assert volume_min <= dimensions_max ** 3
    
    while True:
        box = Box(size=Vector3D(
                x=randint(dimensions_min, dimensions_max),
                y=randint(dimensions_min, dimensions_max),
                z=randint(dimensions_min, dimensions_max),
        ))
        if box.volume >= volume_min:
            return box


def generate_boxes_for_player() -> List[Box]:
    volume_remaining = config.container_size['x'] * config.container_size['y'] * config.container_size['z']
    volume_remaining *= config.boxes['volume_provisioning']
    volume_remaining = int(math.ceil(volume_remaining / 2))
    
    log.debug(f'generating boxes for a player: starting, target volume: {volume_remaining}')
    boxes = []
    while volume_remaining > 0:
        b = generate_single_box()
        boxes.append(b)
        volume_remaining -= b.volume
        log.debug(f'generating boxes for a player, remaining volume: {volume_remaining}')
        
    log.debug('generating boxes for a player: finished')
    return boxes


if __name__ == '__main__':
    log.basicConfig(level=config.log_level)
    
    log.info('starting game')
    initial_boxes = [Box(size=Vector3D(1, 2, 3))] * 20
    game_state = GameState(
            container_size=Vector3D(10, 10, 10),
            boxes_player1=generate_boxes_for_player(),
            boxes_player2=generate_boxes_for_player(),
    )
    
    game_state.run_game()
