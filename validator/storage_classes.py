from dataclasses import dataclass


@dataclass
class Vector3D:
    x: int
    y: int
    z: int
    
    def __repr__(self):
        return f'Vector3D(x={self.x}, y={self.y}, z={self.z})'
    
    def smallest_dimension(self) -> int:
        return min(self.x, self.y, self.z)
    
    def as_size_dict(self) -> dict:
        return {'width': self.x, 'height': self.y, 'length': self.z}
    
    def as_position_dict(self) -> dict:
        return {'x': self.x, 'y': self.y, 'z': self.z}
    
    @staticmethod
    def from_size_dict(size: dict):
        return Vector3D(x=size['width'], y=size['height'], z=size['length'])
    
    @staticmethod
    def from_position_dict(pos: dict):
        return Vector3D(x=pos['x'], y=pos['y'], z=pos['z'])
        

@dataclass
class Box:
    size: Vector3D
    
    @property
    def volume(self) -> int:
        return self.size.x * self.size.y * self.size.z
    
    def __repr__(self):
        return f'Box(width={self.size.x}, height={self.size.y}, length={self.size.z})'
    
    def as_dict(self):
        return self.size.as_size_dict()
    
    @staticmethod
    def from_dict(d: dict):
        return Box(size=Vector3D(x=d['width'], y=d['height'], z=d['length']))
    
    def __eq__(self, other):
        sizes_own = [self.size.x, self.size.y, self.size.z]
        sizes_other = [other.size.x, other.size.y, other.size.z]
        return sorted(sizes_own) == sorted(sizes_other)


@dataclass
class BoxPlacement:
    box: Box
    pos: Vector3D
    
    def validate_no_intersection(self, other) -> bool:
        """
        Test if this placement intersects with another placement
        
        https://math.stackexchange.com/a/2651718
        
        :param other: BoxPlacement
        """
        
        intersection_on_x = False
        intersection_on_y = False
        intersection_on_z = False
        
        if self.pos.x == other.pos.x \
                or self.pos.x < other.pos.x < self.pos.x + self.box.size.x \
                or other.pos.x < self.pos.x < other.pos.x + other.box.size.x\
                or self.pos.x < other.pos.x + other.box.size.x < self.pos.x + self.box.size.x\
                or other.pos.x < self.pos.x + self.box.size.x < other.pos.x + other.box.size.x:
            intersection_on_x = True
        
        if self.pos.y == other.pos.y \
                or self.pos.y < other.pos.y < self.pos.y + self.box.size.y \
                or other.pos.y < self.pos.y < other.pos.y + other.box.size.y\
                or self.pos.y < other.pos.y + other.box.size.y < self.pos.y + self.box.size.y\
                or other.pos.y < self.pos.y + self.box.size.y < other.pos.y + other.box.size.y:
            intersection_on_y = True
        
        if self.pos.z == other.pos.z \
                or self.pos.z < other.pos.z < self.pos.z + self.box.size.z \
                or other.pos.z < self.pos.z < other.pos.z + other.box.size.z\
                or self.pos.z < other.pos.z + other.box.size.z < self.pos.z + self.box.size.z\
                or other.pos.z < self.pos.z + self.box.size.z < other.pos.z + other.box.size.z:
            intersection_on_z = True
            
        if intersection_on_x and intersection_on_y and intersection_on_z:
            raise ValueError(f'two boxes are intersecting (placement 1: {self}, placement 2: {other}')
