import logging as log
from typing import List, Literal, Callable, Union
import time
import random
import json

import requests

from storage_classes import Vector3D, Box, BoxPlacement
import config


class GameState:
    _container_size: Vector3D
    _placed_boxes: List[BoxPlacement] = []
    _player1_remaining_boxes: List[Box]
    _player2_remaining_boxes: List[Box]
    _active_player: Literal[1, 2, None] = None
    
    @property
    def container_size(self):
        return self._container_size
    
    @property
    def placed_boxes(self):
        return self._placed_boxes
    
    @property
    def player1_remaining_boxes(self):
        return self._player1_remaining_boxes
    
    @property
    def player2_remaining_boxes(self):
        return self._player2_remaining_boxes
    
    @property
    def active_player(self):
        return self._active_player
    
    def __init__(self, container_size: Vector3D, boxes_player1: List[Box], boxes_player2: List[Box]):
        assert container_size.smallest_dimension() > 0, 'Container has to be of positive size in all dimensions'
        self._container_size = container_size
        
        # sanity check over all boxes
        map(self.validate_box, boxes_player1)
        map(self.validate_box, boxes_player2)
        self._player1_remaining_boxes = boxes_player1
        self._player2_remaining_boxes = boxes_player2
    
    # noinspection PyMethodMayBeStatic
    def validate_box(self, box: Box):
        assert box.size.smallest_dimension() > 0, 'container has to be of positive size in all dimensions'
    
    def validate_box_placement(self, placement: BoxPlacement):
        self.validate_box(placement.box)
        assert placement.pos.smallest_dimension() >= 0, 'positions <0 in any dimension are not allowed'
        assert placement.pos.x + placement.box.size.x <= self._container_size.x, 'box exceeds container in x dimension'
        assert placement.pos.y + placement.box.size.y <= self._container_size.y, 'box exceeds container in y dimension'
        assert placement.pos.z + placement.box.size.z <= self._container_size.z, 'box exceeds container in z dimension'
        
        # test for intersections with other boxes
        for placed_box in self._placed_boxes:
            placement.validate_no_intersection(placed_box)
    
    @staticmethod
    def check_if_player_ready(url) -> bool:
        try:
            r = requests.get(url + '/ping', timeout=1)
            return r.ok
        except requests.exceptions.RequestException:
            pass
        return False
    
    def _wait_until_players_ready(self):
        time_end = time.monotonic() + 30
        player1_ready = False
        player2_ready = False
        
        while time_end >= time.monotonic():
            if player1_ready is False:
                self._active_player = 1
                player1_ready = self.check_if_player_ready(config.player1_url)
            if player2_ready is False:
                self._active_player = 2
                player2_ready = self.check_if_player_ready(config.player2_url)
            if player1_ready and player2_ready:
                log.info(f'Players ready check: both players are ready, {time_end - time.monotonic()}s remaining')
                self._active_player = None
                return
        
        raise Exception('Players ready check: at least one player wasn\'t ready and the waiting time is up. '
                        f'player1 ready? {player1_ready}; player2 ready? {player2_ready}')
    
    def get_game_state_for_player(self, player: Literal[1, 2]) -> dict:
        if player == 1:
            parcels_own = self._player1_remaining_boxes
            parcels_opponent = self._player2_remaining_boxes
        else:
            parcels_own = self._player2_remaining_boxes
            parcels_opponent = self._player1_remaining_boxes
            
        # we're building the state as a string and convert them back as an object to be able to use the __repr__
        #   functions of components but also be able to easily transmit them as JSON (instead of a JSON string
        #   containing string we're building here).
        return {
            "container": self._container_size.as_size_dict(),
            "parcels": list(map(Box.as_dict, parcels_own)),
            "opponent_parcels": list(map(Box.as_dict, parcels_opponent)),
        }
    
    @staticmethod
    def _determine_looser_on_error(f: Callable):
        def wrapper(*args, **kwargs):
            log.debug(args[0])
            assert len(args) >= 1 and isinstance(args[0], GameState),\
                'The first argument of the decorated function has to be an instance of GameState'
            self = args[0]
            
            try:
                f(*args, **kwargs)
            except BaseException as e:
                if self._active_player is not None:
                    log.info(f'–––––> Player {self._active_player} lost the game by causing the following error: {e!r}', exc_info=e)
                else:
                    log.error('An error occurred but no player was active, raising the error again.')
                    raise e
        
        return wrapper
    
    def get_active_players_url(self) -> str:
        if self._active_player is None:
            log.warning(f'tried to get URL of active player but no player is currently active')
            return ''
        
        if self._active_player == 1:
            return config.player1_url
        else:
            return config.player2_url
        
    def _swap_active_players(self):
        if self._active_player == 1:
            self._active_player = 2
        else:
            self._active_player = 1
    
    def _run_round(self):
        # fetch box placed by the current player
        log.info(f'requesting next placement from player {self._active_player}')
        next_parcel = requests.get(self.get_active_players_url() + '/next-parcel', timeout=config.timout_default)
        log.debug(f'received the following response: {next_parcel.text}')
        assert next_parcel.ok, f'Response code ({next_parcel.status_code}) indicates an error'
        placement = BoxPlacement(
                box=Box.from_dict(next_parcel.json()['parcel']),
                pos=Vector3D.from_position_dict(next_parcel.json()['position'])
        )
        self.validate_box_placement(placement)
        self._placed_boxes.append(placement)
        
        self._swap_active_players()
        
        # tell other player the placed box
        opponent_parcel = requests.post(
                url=self.get_active_players_url() + '/opponent-parcel',
                json=next_parcel.json(),
                timeout=config.timout_default,
        )
        assert opponent_parcel.ok, f'Response code ({opponent_parcel.status_code}) indicates an error'
    
    @_determine_looser_on_error
    def run_game(self):
        self._wait_until_players_ready()
        
        self._active_player = 1
        player1_state = self.get_game_state_for_player(player=1)
        log.debug(f'sending new game state to player 1:\n{json.dumps(player1_state)}')
        assert requests.post(
                url=config.player1_url + '/new-game',
                json=player1_state,
                timeout=config.timout_default
        ).ok
        
        self._active_player = 2
        player2_state = self.get_game_state_for_player(player=2)
        log.debug(f'sending new game state to player 2:\n{json.dumps(player2_state)}')
        assert requests.post(
                url=config.player2_url + '/new-game',
                json=player2_state,
                timeout=config.timout_default
        ).ok
        
        # chose beginning player randomly
        # noinspection PyTypeChecker
        self._active_player = random.choice([1, 2])
        
        while True:
            self._run_round()
