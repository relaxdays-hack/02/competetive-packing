FROM mcr.microsoft.com/dotnet/sdk:5.0 as builder
WORKDIR /src
COPY ./CompetetivePacking ./CompetetivePacking
COPY ./CompetetivePacking.Test ./CompetetivePacking.Test
COPY ./competetive-packing.sln ./
RUN dotnet build --nologo -c RELEASE \
        CompetetivePacking/CompetetivePacking.csproj && \
    dotnet publish --nologo -c RELEASE -o /app \
        CompetetivePacking/CompetetivePacking.csproj

FROM mcr.microsoft.com/dotnet/runtime:5.0
WORKDIR /app
COPY --from=builder /app /app
ENV IS_DOCKER=1
EXPOSE 8080
CMD [ "dotnet", "/app/CompetetivePacking.dll" ]
