using System;
using System.Collections.Generic;
using CompetetivePacking.Data;
using Priority_Queue;

namespace CompetetivePacking.Game
{
    public class Option
    {
        public Job Job { get; }

        public int ParcelId { get; }

        public Vector Boundary { get; set; }

        public Vector FloodStart { get; set; }

        public float Score { get; set; }

        public Option(Job job, int parcelId, Vector boundary, Vector floodStart, float score)
        {
            Job = job;
            ParcelId = parcelId;
            Boundary = boundary;
            FloodStart = floodStart;
            Score = score;
        }

        public bool StillValid(Container container, Player.ParcelManagement management)
        {
            return container.CanPlace(Job) && management.IsAvailable(ParcelId);
        }

        public override string ToString()
        {
            return $"Job: ({Job}), Boundary: {Boundary}, FloodStart: {FloodStart}, Score: {Score}";
        }

        public override bool Equals(object? obj)
        {
            return obj is Option option &&
                   EqualityComparer<Job>.Default.Equals(Job, option.Job) &&
                   ParcelId == option.ParcelId &&
                   EqualityComparer<Vector>.Default.Equals(Boundary, option.Boundary) &&
                   EqualityComparer<Vector>.Default.Equals(FloodStart, option.FloodStart) &&
                   Score == option.Score;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Job, ParcelId, Boundary, FloodStart, Score);
        }
    }
}