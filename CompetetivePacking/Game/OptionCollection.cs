using System;
using System.Collections.Generic;
using Priority_Queue;
using System.Threading;
using System.Threading.Tasks;

namespace CompetetivePacking.Game
{
    public class OptionCollection
    {
        private SortedList<float, HashSet<Option>> queue = new SortedList<float, HashSet<Option>>();

        private readonly SemaphoreSlim lockQueue = new SemaphoreSlim(1, 1);

        private readonly Data.Container container;

        private readonly Player.ParcelManagement management;

        private int flushCounter;

        public OptionCollection(Data.Container container, Player.ParcelManagement management)
        {
            this.container = container;
            this.management = management;
        }

        public int Fill { get; private set; }

        public async Task AddOption(Option option)
        {
            var flush = flushCounter;
            if (!option.StillValid(container, management))
                return;
            await lockQueue.WaitAsync().ConfigureAwait(false);
            try 
            {
                // check if the container has been changed
                if (flushCounter != flush && !option.StillValid(container, management))
                    return;
                if (!queue.TryGetValue(option.Score, out HashSet<Option>? row))
                    queue.Add(option.Score, row = new HashSet<Option>());
                row.Add(option);
                Fill++;
            }
            finally
            {
                lockQueue.Release();
            }
        }

        public async Task<Option?> GetBestOption()
        {
            await lockQueue.WaitAsync().ConfigureAwait(false);
            try
            {
                Option? result = null;
                foreach (var (score, row) in queue)
                {
                    foreach (var entry in row)
                    {
                        Fill--;
                        result = entry;
                        row.Remove(entry);
                        break;
                    }
                    if (result is not null)
                    {
                        if (row.Count == 0)
                            queue.Remove(score);
                        break;
                    }
                }
                return result;
            }
            finally
            {
                lockQueue.Release();
            }
        }

        public async Task Cleanup()
        {
            await lockQueue.WaitAsync().ConfigureAwait(false);
            try
            {
                flushCounter++;
                var fill = 0;
                var newQueue = new SortedList<float, HashSet<Option>>(queue.Count);
                foreach (var (score, row) in queue)
                {
                    row.RemoveWhere(entry => !entry.StillValid(container, management));
                    if (row.Count > 0)
                        newQueue.Add(score, row);
                    fill += row.Count;
                }
                queue = newQueue;
                Fill = fill;
            }
            finally
            {
                lockQueue.Release();
            }
        }
    }
}