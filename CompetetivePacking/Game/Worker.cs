using System;
using CompetetivePacking.Data;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace CompetetivePacking.Game
{
    public class Worker
    {
        public Player.ParcelManagement Parcels { get; }

        public OptionCollection Options { get; }

        public Container Container { get; }

        private readonly Random random;

        public int Id { get; }

        public Worker(int index, Player.ParcelManagement parcels, OptionCollection options, Container container)
        {
            Id = index;
            Parcels = parcels;
            Options = options;
            Container = container;
            random = new Random(HashCode.Combine(
                index,
                Environment.TickCount, 
                System.Threading.Thread.CurrentThread.ManagedThreadId
            ));
        }

        public Task ExecuteAsync()
        {
            return Task.Run(async () => 
            {
                while (Parcels.IsAnythingAvailable())
                {
                    if (Options.Fill > 500_000)
                        await Task.Delay(1).ConfigureAwait(false);
                    else await GenerateSuggestion().ConfigureAwait(false);
                }
            });
        }

        private async Task GenerateSuggestion()
        {
            // generate a random position
            var randPos = GetPosition();
            // check if valid position
            var job = new Job(Vector.One, randPos);
            if (!Container.CanPlace(job))
                return;
            // extend to maximum
            job = Extend(job);
            // get a possible block for it
            // var hasOption = false;
            foreach (var parcel in GetParcels(job.Parcel))
            {
                var jiggle = GetJiggle(parcel.parcel, job.Parcel);
                // create suggestion
                var suggestion = new Option(
                    MoveDown(new Job(parcel.parcel, job.Position + jiggle)),
                    parcel.parcelId,
                    job.Parcel,
                    randPos,
                    parcel.score
                );
                // Serilog.Log.Verbose("Worker [{id}]: Add suggestion {suggestion}", Id, suggestion);
                await Options.AddOption(suggestion).ConfigureAwait(false);
                // hasOption = true;
            }
            // if (!hasOption)
                // Serilog.Log.Debug("Worker [{id}]: Got no option for {job} (start: {start})", Id, job, randPos);
        }

        private Vector GetJiggle(Vector parcelSize, Vector boundary)
        {
            return new Vector(
                random.Next(0, boundary.X - parcelSize.X),
                random.Next(0, boundary.Y - parcelSize.Y),
                random.Next(0, boundary.Z - parcelSize.Z)
            );
        }

        private Vector GetPosition()
        {
            return new Vector(
                random.Next(0, Container.Size.X),
                random.Next(0, Container.Size.Y),
                random.Next(0, Container.Size.Z)
            );
        }

        private Job Extend(Job job)
        {
            Span<Vector> units = new[]
            {
                // extend x
                Vector.UnitX, -Vector.UnitX,
                // extend y
                Vector.UnitY, -Vector.UnitY,
                // extend z
                Vector.UnitZ, -Vector.UnitZ,
            };
            // bring the vectors in a random order
            for (int i = 0; i < units.Length; ++i)
            {
                var j = random.Next(i, units.Length);
                if (i != j)
                    (units[i], units[j]) = (units[j], units[i]);
            }
            // extend the axis in the new order
            for (int i = 0; i < units.Length; ++i)
            {
                var orig = job;
                job = Extend(job, units[i]);
                // Serilog.Log.Verbose("Worker [{Id}]: Extend ({jobPrev}) to ({jobNew}) with {unit}", Id, orig, job, units[i]);
            }
            // no more extensions are possible
            return job;
        }

        private Job Extend(Job job, Vector inc)
        {
            Job next = job;
            do
            {
                next = (job = next).Extend(inc);
            }
            while (Container.CanPlace(next));
            return job;
        }
    
        private Job MoveDown(Job job)
        {
            Job next = job;
            do
            {
                job = next;
                next = new Job(job.Parcel, job.Position - Vector.UnitY);
            }
            while (Container.CanPlace(next));
            return job;
        }
    
        private IEnumerable<(Vector parcel, int parcelId, float score)> GetParcels(Vector boundary)
        {
            var rotations = Vector.AvailableRotations;
            for (int i = 0; i < rotations.Length; ++i)
            {
                var rotation = rotations.Span[i];
                var result = Parcels.GetParcel(boundary.Rotate(rotation));
                if (result is null)
                    continue;
                yield return (result.Value.parcel.RotateInverse(rotation), result.Value.parcelId, result.Value.score);
            }
        }
    }
}