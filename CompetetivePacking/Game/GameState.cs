using System;
using System.Threading.Tasks;
using CompetetivePacking.Data;

namespace CompetetivePacking.Game
{
    public class GameState
    {
        private readonly Player.ParcelManagement player;
        private readonly Container container;
        private readonly OptionCollection options;
        private readonly Task[] executors;

        public GameState(NewGame game)
        {
            Serilog.Log.Debug("Initialize Game");
            player = new Player.ParcelManagement(game.Parcels);
            container = new Container(game.Container);
            options = new OptionCollection(container, player);
            executors = new Task[Environment.ProcessorCount * 2 * 0 + 1];
            Serilog.Log.Debug("Start worker");
            for (int i = 0; i< executors.Length; ++i)
                executors[i] = new Worker(i, player, options, container).ExecuteAsync();
            Serilog.Log.Debug("Game Running");
        }

        public async Task Fill(Job job, bool enemy)
        {
            Serilog.Log.Debug("Fill job {job} (enemy: {enemy})", job, enemy);
            container.Place(job);
            if (!enemy)
                player.SetUsed(job.Parcel);
            await options.Cleanup().ConfigureAwait(false);
        }

        public async Task<Job?> GetMove()
        {
            var move = await options.GetBestOption().ConfigureAwait(false);
            if (move is null)
            {
                // Serilog.Log.Warning("Cannot get new move");
                return null;
            }
            await Fill(move.Job, false).ConfigureAwait(false);
            Serilog.Log.Debug("Our next move: {job}", move.Job);
            return move.Job;
        }

        public bool CanMove()
        {
            return player.IsAnythingAvailable();
        }
    }
}