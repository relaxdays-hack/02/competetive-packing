using System;
using System.Collections.Generic;
using CompetetivePacking.Data;
using System.Threading.Tasks;
using System.Linq;

namespace CompetetivePacking.Player
{
    public class ParcelManagement
    {
        private Halbordnung halbordnung;
        private Memory<int> availableParcelPos;
        private ReadOnlyMemory<Vector> parcels;
        private int ParcelCnt;

        private Func<Vector,Vector,float> scoreFn;

        /// <summary>
        /// Initialize class and prepare parcels for usage.
        /// Using the default scoring: Volume.
        /// </summary>
        /// <param name="parcels">the list of all parcel sizes</param>
        public ParcelManagement(ReadOnlyMemory<Vector> parcels) 
            : this(parcels, Scoring.ScoreFn.Volume) {
        }

        public ParcelManagement(ReadOnlyMemory<Vector> parcels, Scoring.ScoreFn scoreFn) {
            SortedDictionary<Vector,int> parcelsWithCount = new SortedDictionary<Vector, int>(); //parcels.Length);
            foreach (Vector parcel in parcels.Span) {
                if (parcelsWithCount.TryGetValue(parcel, out int count)) {
                    count++;
                } else {
                    count=1;
                }
                parcelsWithCount[parcel] = count;
            }
            this.parcels = parcelsWithCount.Keys.ToArray();
            availableParcelPos = parcelsWithCount.Values.ToArray();

            halbordnung = new Halbordnung(this.parcels);

            ParcelCnt = parcelsWithCount.Count;

            this.scoreFn = Scoring.getScoreFunc(scoreFn);
        }

        /// <summary>
        /// Mark a single parcel as used so that is no longer returned as suggestion
        /// </summary>
        /// <param name="parcel">the size of parcel that is used</param>
        public void SetUsed(Vector parcel) {
            int parcelPos = halbordnung.getPositionOf(parcel);
            availableParcelPos.Span[parcelPos]--;
        }

        public bool IsAvailable(int parcelId)
        {
            return availableParcelPos.Span[parcelId] > 0;
        }

        public bool IsAnythingAvailable()
        {
            foreach (var entry in availableParcelPos.Span)
                if (entry > 0)
                    return true;
            return false;
        }

        /// <summary>
        /// get the best parcel size for the current boundary. This includes the rotation.
        /// </summary>
        /// <param name="boundary">the current boundary size</param>
        /// <returns>
        /// If found it returns the parcel (not including rotation) and the calculated score for this fit.
        /// </returns>
        public (Vector parcel, int parcelId, float score)? GetParcel(Vector boundary) {
            SortedSet<int> smallerParcelPos = halbordnung.getAllSmallerParcelPos(boundary);
            float minScore = float.MaxValue;
            (Vector size, int id)? bestParcel = null;
            foreach (var parcelPos in smallerParcelPos) {
                if (availableParcelPos.Span[parcelPos] <= 0) {
                    continue;
                }
                var parcel = halbordnung.getParcelAt(parcelPos);
                float currScore = scoreFn(parcel, boundary);
                if( currScore < minScore) {
                    bestParcel = (parcel,parcelPos);
                    minScore = currScore;
                }
            }
            if (bestParcel == null) {
                return null;
            } else {
                return (bestParcel.Value.size, bestParcel.Value.id, minScore);
            }
        }
    }
}