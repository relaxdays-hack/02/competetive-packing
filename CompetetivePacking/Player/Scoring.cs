
using CompetetivePacking.Data;
using System;

namespace CompetetivePacking.Player {

    public class Scoring {

        // Score changed: lower score = better

        public enum ScoreFn {
            PercentFilled,
            Volume,
            ArithmeticMean,
            GeometricMean,
            Variance
        }

        public static Func<Vector,Vector,float> getScoreFunc(ScoreFn scoreFn) {
            return scoreFn switch {
                ScoreFn.PercentFilled => percentFilled,
                ScoreFn.Volume => (parcel, _) => volume(parcel),
                ScoreFn.ArithmeticMean => (parcel, _) => arithmeticMean(parcel),
                ScoreFn.GeometricMean => (parcel, _) => geometricMean(parcel),
                ScoreFn.Variance => (parcel, _) => variance(parcel),
                _ => throw new ArgumentException("undefined eneum value"),
            };
        }

        public static float percentFilled (Vector outerParcel, Vector innerParcel) {
            float maxVolume = volume(outerParcel);
            float usedVolume = volume(innerParcel);
            return 1.0f - usedVolume / maxVolume;
        }

        public static float volume (Vector parcel) {
            float volume = parcel.X * parcel.Y * parcel.Z;
            return -volume;
        }

        public static float variance (Vector parcel) {
            double mean = arithmeticMeanDbl(parcel);
            double xDist = Math.Abs(mean - parcel.X);
            double qXDist = xDist * xDist;
            double yDist = Math.Abs(mean - parcel.Y);
            double qYDist = yDist * yDist;
            double zDist = Math.Abs(mean - parcel.Z);
            double qZDist = zDist * zDist;
            double variance = qXDist + qYDist + qZDist;
            return (float) -variance;
        }

        public static float arithmeticMean (Vector parcel) {
            float sum = parcel.X + parcel.Y + parcel.Z;
            return sum / -3.0f;
        }
        public static double arithmeticMeanDbl (Vector parcel) {
            double sum = parcel.X + parcel.Y + parcel.Z;
            return sum / -3.0;
        }

        public static float geometricMean (Vector parcel) {
            float product = parcel.X * parcel.Y * parcel.Z;
            float geometricMean = (float) Math.Pow(product, 1.0f/-3.0f);
            return geometricMean;
        }
    }
}