// This class implelements a half ordering of (sortet) sets of size (exactly) 3 of ints

using System;
using System.Collections.Generic;
using CompetetivePacking.Data;

namespace CompetetivePacking.Player {

    public class Halbordnung {
        
        private ReadOnlyMemory <Vector> allParcells;
        private SortedList <Vector,int> allParcellsSorted;
        public bool?[,] isLarger;

        private int ParcelCnt; // pairwise different parcels
        private SortedSet<int>[] smallerParcels;
        private SortedSet<int>[] largerParcels;

        public Halbordnung (ReadOnlyMemory<Vector> allParcells) {
            this.allParcells = allParcells;
            // the two calls could also be made parallel, also sorting should be done parallel
            ParcelCnt = allParcells.Length;
            allParcellsSorted = new SortedList<Vector, int>(ParcelCnt);
            addAllToSortetList();
            isLarger = new bool?[ParcelCnt, ParcelCnt];
            isLarger.Initialize();
            smallerParcels = new SortedSet<int>[ParcelCnt];
            smallerParcels.Init(_ => new SortedSet<int>());
            largerParcels = new SortedSet<int>[ParcelCnt];
            largerParcels.Init(_ => new SortedSet<int>());
        }

        public Vector getParcelAt (int parcelPos) {
            return allParcells.Span[parcelPos];
        }


        // O(log(n)) runtime
        // returns the position of the parcel. If the parcel is not present it returns -1;
        public int getPositionOf (Vector parcel) {
            bool contains=allParcellsSorted.TryGetValue(parcel, out int pos);
            if(!contains) {
                pos = -1;
            }
            return pos;
        }

        // returns true if outerParcel can contain innerParcel,
        // which means all dimension of inner Parcel are <= outher Parcel
        // if the value is already stored it will return it if not it will compute and store it
        public bool canContain (int outerParcelPos, int innerParcelPos) {
            if (outerParcelPos == innerParcelPos) {
                return true;
            }
            bool? cachedResult = isLarger[outerParcelPos, innerParcelPos];
            if (cachedResult == null){
                Vector outerParcel = allParcells.Span[outerParcelPos];
                Vector innerParcel = allParcells.Span[innerParcelPos];
                bool result = computeCanContainExplicitly(outerParcel, innerParcel);
                cacheResult(outerParcelPos, innerParcelPos, result);
                return result;
            } else {
                return cachedResult.Value;
            }
        }

        // returns true if outerParcel can contain innerParcel,
        // which means all dimension of inner Parcel are <= outher Parcel
        // if the value is already stored it will return it if not it will compute and store it
        [Obsolete("O(log(n)) runtime for n elements in allParcels, pls save the parcelIDs in your program and use them")]
        public bool canContain (Vector outerParcel, Vector innerParcel) {
            int outerParcelPos = getPositionOf(outerParcel);
            int innerParcelPos = getPositionOf(innerParcel);
            if (outerParcelPos == innerParcelPos) {
                return true;
            }
            if (outerParcelPos == -1 || innerParcelPos == -1){
                throw new Exception("Parcel is not contained in this set.");
            }
            bool? cachedResult = isLarger[outerParcelPos, innerParcelPos];
            if (cachedResult == null){
                return computeCanContainExplicitly(outerParcel, innerParcel);
            } else {
                return cachedResult.Value;
            }
        }

        // returns true if outerParcel can contain innerParcel,
        // which means all dimension of inner Parcel are <= outher Parcel
        private bool computeCanContainExplicitly (Vector outerParcel, Vector innerParcel) {
            Vector outerParcelSorted = outerParcel.Sorted;
            Vector innerParcelSorted = innerParcel.Sorted;
            int outerX = outerParcelSorted.X;
            int innerX = innerParcelSorted.X;
            int outerY = outerParcelSorted.Y;
            int innerY = innerParcelSorted.Y;
            int outerZ = outerParcelSorted.Z;
            int innerZ = innerParcelSorted.Z;
            if (outerX >= innerX && outerY >= innerY && outerZ >= innerZ) {
                return true;
            } else {
                return false;
            }
        }

        // adds the relation of outerParcel to innerParcel to the halfordering
        // will also update predesessors and succsessors
        // will do nothing if outerParcelPos == innerParcelPos is true
        private void cacheResult (int outerParcelPos, int innerParcelPos, bool outerContainInner) {
            if (outerParcelPos == innerParcelPos) {
                return;
            }
            isLarger[outerParcelPos, innerParcelPos] = outerContainInner;
            if (outerContainInner) {
                largerParcels[innerParcelPos].Add(outerParcelPos);
                smallerParcels[outerParcelPos].Add(innerParcelPos);
            }
        }

        private void addAllToSortetList () {
            for (int i = 0; i < allParcells.Length; i++) {
                allParcellsSorted.Add(getParcelAt(i),i);
            }
        }

        public SortedSet<int> getLarger(int parcelPos) {
            return largerParcels[parcelPos];
        }
        public SortedSet<int> getSmaller(int parcelPos) {
            return smallerParcels[parcelPos];
        }

        // populates the full relation matrix (isSmaller) and builds the smaller and larger SortedSets
        // The diagonal (i=i) is not set
        // WARING: costly operation
        public void computeFullRelation() {
            for (int outerParcelPos = 0; outerParcelPos < ParcelCnt; outerParcelPos++) {
                for (int innerParcelPos = 0; innerParcelPos < ParcelCnt; innerParcelPos++) {
                    if (innerParcelPos == outerParcelPos) {
                        continue;
                    }
                    if (isLarger[outerParcelPos, innerParcelPos] == null) {
                        Vector outerParcel = getParcelAt(outerParcelPos);
                        Vector innerParcel = getParcelAt(innerParcelPos);
                        bool result = computeCanContainExplicitly(outerParcel,innerParcel);
                        cacheResult(outerParcelPos, innerParcelPos, result);
                    }   
                }
            }
        }

        // O(ParcelCnt) runtime
        public SortedSet<int> getAllSmallerParcelPos (Vector parcel) {
            SortedSet<int> allSmaller = new SortedSet<int>();
            foreach (var (knownParcel, parcelPos) in allParcellsSorted) {
                if(canContain(parcel.Sorted, parcelPos)) {
                    allSmaller.Add(parcelPos);
                }
            }
            return allSmaller;
        }

        private bool canContain(Vector outerParcel, int innerParcelPos) {
            Vector innerParcel = getParcelAt(innerParcelPos);
            return computeCanContainExplicitly(outerParcel,innerParcel);
        }
    }
}