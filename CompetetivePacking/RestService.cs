using System;
using System.Text.Json;
using MaxLib.WebServer;
using MaxLib.WebServer.Api.Rest;
using MaxLib.WebServer.Post;
using System.Threading.Tasks;
using CompetetivePacking.Data;
using System.IO;

namespace CompetetivePacking
{
    public class RestService
    {
        class PostJsonRule : ApiRule
        {
            public string Target { get; }

            public PostJsonRule(string name)
                => Target = name;

            public override bool Check(RestQueryArgs args)
            {
                if (args.Post.Data is UnknownPostData data && data.MimeType == MimeType.ApplicationJson)
                {
                    // System.IO.File.WriteAllBytes("request.json", data.Data.ToArray());
                    var reader = new Utf8JsonReader(data.Data.Span);
                    try
                    {
                        if (!JsonDocument.TryParseValue(ref reader, out JsonDocument? document))
                            return false;
                        args.ParsedArguments[Target] = document;
                    }
                    catch (JsonException e)
                    {
                        Serilog.Log.Error(e, "cannot parse json");
                        return false;
                    }
                    return true;
                }
                else return false;
            }
        }

        public RestApiService BuildService()
        {
            var api = new RestApiService();
            var fact = new ApiRuleFactory();
            api.RestEndpoints.AddRange(new[]
            {
                RestActionEndpoint.Create(Ping)
                    .Add(fact.Location(
                        fact.UrlConstant("ping"),
                        fact.MaxLength()
                    )),
                RestActionEndpoint.Create<JsonDocument>(NewGameAsync, "post")
                    .Add(fact.Location(
                        fact.UrlConstant("new-game"),
                        fact.MaxLength()
                    ))
                    .Add(new PostJsonRule("post")),
                RestActionEndpoint.Create<JsonDocument>(OpponentParcelAsync, "post")
                    .Add(fact.Location(
                        fact.UrlConstant("opponent-parcel"),
                        fact.MaxLength()
                    ))
                    .Add(new PostJsonRule("post")),
                RestActionEndpoint.Create(NextParcelAsync)
                    .Add(fact.Location(
                        fact.UrlConstant("next-parcel"),
                        fact.MaxLength()
                    )),
            });

            return api;
        }

        private static Task<HttpDataSource> Ping()
        {
            return Task.FromResult<HttpDataSource>(new HttpStringDataSource(
                $"Server is running since {Program.Startup}"
            ));
        }

        private static Game.GameState? game;

        private static async Task<HttpDataSource> NewGameAsync(JsonDocument document)
        {
            try 
            {
                if (!Data.NewGame.TryCreate(document.RootElement, out NewGame? newGame))
                    return new HttpStringDataSource("invalid data");
                    
                game = new Game.GameState(newGame);
                await Task.CompletedTask;

                return new HttpStringDataSource("ok");
            }
            catch (Exception e)
            {
                return new HttpStringDataSource(e.Message);
            }
        }

        private static async Task<HttpDataSource> OpponentParcelAsync(JsonDocument document)
        {
            if (!Data.Job.TryCreate(document.RootElement, out Job? job))
                return new HttpStringDataSource("invalid data");
            
            if (game is Game.GameState state)
            {
                await state.Fill(job.Value, true).ConfigureAwait(false);
                return new HttpStringDataSource("ok");
            }
            else 
            {
                return new HttpStringDataSource("not initialized");
            }
        }

        private static async Task<HttpDataSource> NextParcelAsync()
        {
            var started = DateTime.UtcNow;
            await Task.CompletedTask;

            Job? job = null;
            if (game is Game.GameState state)
            {
                while (job is null && state.CanMove())
                {
                    job = await state.GetMove().ConfigureAwait(false);
                    if (job is null)
                        // wait a small amount of time and try to fetch the move again. The process
                        // is killed if we can't find a job in 5 seconds. Don't let this happen!
                        await Task.Delay(1).ConfigureAwait(false); // PANIC!!!
                }
            }
            job ??= new Job(new Vector(0, 0, 0), new Vector(0, 0, 0));

            var m = new MemoryStream();
            var writer = new Utf8JsonWriter(m, new JsonWriterOptions
            {
                Indented = true,
            });
            job?.Write(writer);
            await writer.FlushAsync().ConfigureAwait(false);
            m.Position = 0;

            var finish = DateTime.UtcNow;
            var diff = finish - started;
            var max = TimeSpan.FromSeconds(4);
            if (diff < max)
                await Task.Delay(max - diff).ConfigureAwait(false);

            return new HttpStreamDataSource(m)
            {
                MimeType = MimeType.ApplicationJson,
            };
        }
    }
}