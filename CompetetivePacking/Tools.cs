using System;
using System.Collections.Generic;

namespace CompetetivePacking
{
    public static class Tools
    {
        /// <summary>
        /// Initialize an array cleanly with a fresh value. 
        /// No more <see cref="NullReferenceException" />s!
        /// </summary>
        /// <param name="array">the array to initialize</param>
        /// <param name="value">the fresh value generator</param>
        /// <typeparam name="T">the type of the array</typeparam>
        public static void Init<T>(this Array array, Func<int, T> value)
        {
            for (int i = 0; i < array.Length; ++i)
                array.SetValue(value(i), i);
        }

        /// <summary>
        /// Initialize an array cleanly with a fresh value. 
        /// No more <see cref="NullReferenceException" />s!
        /// </summary>
        /// <param name="array">the array to initialize</param>
        /// <param name="value">the fresh value generator</param>
        /// <typeparam name="T">the type of the array</typeparam>
        public static void Init<T>(this Array array, Func<T> value)
            => Init(array, _ => value());

        /// <summary>
        /// Returns the content of a list using its ToString method in a nice long string
        /// </summary>
        /// <param name="list">the list to convert</param>
        /// <typeparam name="T">the value type of the items</typeparam>
        /// <returns>the long string</returns>
        public static string ToListString<T>(this IList<T> list)
        {
            var sb = new System.Text.StringBuilder();
            sb.Append("[ ");
            foreach (var item in list)
            {
                sb.Append(item);
                sb.Append(", ");
            }
            sb.Append(" ]");
            return sb.ToString();
        }
    }
}