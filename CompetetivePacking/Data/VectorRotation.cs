namespace CompetetivePacking.Data
{
    public enum VectorRotation
    {
        XYZ,
        XZY,
        YXZ,
        YZX,
        ZXY,
        ZYX,
    }
}