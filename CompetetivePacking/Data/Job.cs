using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json;

namespace CompetetivePacking.Data
{
    public readonly struct Job
    {
        public Vector Parcel { get; }

        public Vector Position { get; }

        public Job(Vector parcel, Vector position)
        {
            Parcel = parcel;
            Position = position;
        }

        public Job Extend(Vector extension)
        {
            var par = Vector.Abs(extension);
            var pos = Vector.Min(Vector.Zero, extension);
            return new Job(par + Parcel, pos + Position);
        }

        public static bool TryCreate(JsonElement json, [NotNullWhen(true)] out Job? value)
        {
            value = default;
            if (!json.TryGetProperty("parcel", out JsonElement node)
                || !Vector.TryCreateDimension(node, out Vector parcel)
                || !json.TryGetProperty("position", out node)
                || !Vector.TryCreatePosition(node, out Vector position)
            )
                return false;
            value = new Job(parcel, position);
            return true;
        }

        public void Write(Utf8JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName("parcel");
            Parcel.WriteDimension(writer);
            writer.WritePropertyName("position");
            Position.WritePosition(writer);
            writer.WriteEndObject();
        }

        public override string ToString()
        {
            return $"Pos: {Position}, Size: {Parcel}";
        }

        public override bool Equals(object? obj)
        {
            return obj is Job job &&
                   EqualityComparer<Vector>.Default.Equals(Parcel, job.Parcel) &&
                   EqualityComparer<Vector>.Default.Equals(Position, job.Position);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Parcel, Position);
        }

        public static bool operator ==(Job left, Job right)
            => left.Equals(right);
        
        public static bool operator !=(Job left, Job right)
            => !left.Equals(right);
    }
}