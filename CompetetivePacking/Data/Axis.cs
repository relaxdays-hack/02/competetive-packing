using System;

namespace CompetetivePacking.Data
{
    public struct Axis
    {
        private readonly ulong data;

        private static Memory<ulong> masks;

        private Axis(ulong data)
        {
            this.data = data;
        }

        static Axis()
        {
            masks = new ulong[64];
            ulong mask = 1;
            for (int i = 0; i < 64; ++i)
            {
                masks.Span[i] = mask;
                mask = (mask << 1) | 1;
            }
        }

        public bool CheckFreePlacement(int start, int length)
        {
            if (length == 0)
                return false;
            if (length < 0 || length > 64)
                throw new ArgumentOutOfRangeException(nameof(length));
            if (start < 0 || start > 64)
                throw new ArgumentOutOfRangeException(nameof(start));
            var mask = masks.Span[length - 1];
            mask <<= start;
            return (data & mask) == 0;
        }

        public Axis Place(int start, int length)
        {
            if (length == 0)
                return this;
            if (length < 0 || length > 64)
                throw new ArgumentOutOfRangeException(nameof(length));
            if (start < 0 || start > 64)
                throw new ArgumentOutOfRangeException(nameof(start));
            var mask = masks.Span[length - 1];
            mask <<= start;
            return new Axis(data | mask);
        }

        public override string ToString()
        {
            var d = BitConverter.GetBytes(data);
            Array.Reverse(d);
            return $"{data:#,#0} (0x{Convert.ToHexString(d)})";
        }
    }
}