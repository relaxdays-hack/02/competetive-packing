using System;
using System.Collections.Generic;
using System.Text.Json;

namespace CompetetivePacking.Data
{
    public readonly struct Vector : IComparable<Vector>
    {
        public int X { get; }

        public int Y { get; }

        public int Z { get; }

        public Vector(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
            var (a, b, c) =
                X >= Y && X >= Z ? (X, Y, Z) :
                Y >= X && Y >= Z ? (Y, X, Z) : 
                                   (Z, X, Y);
            if (b < c)
                (b, c) = (c, b);
            SortedIntern = (a, b, c);
        }

        public Vector Sorted => new Vector(SortedIntern.X, SortedIntern.Y, SortedIntern.Z);

        private readonly (int X,int Y,int Z) SortedIntern;

        public static bool TryCreateDimension(JsonElement element, out Vector value)
        {
            var json = element.ToString();
            value = default;
            if (!element.TryGetProperty("width", out JsonElement node) 
                || !node.TryGetInt32(out int width)
                || !element.TryGetProperty("height", out node)
                || !node.TryGetInt32(out int height)
                || !element.TryGetProperty("length", out node)
                || !node.TryGetInt32(out int length)
            )
                return false;
            value = new Vector(width, height, length);
            return true;
        }

        public static bool TryCreatePosition(JsonElement element, out Vector value)
        {
            value = default;
            if (!element.TryGetProperty("x", out JsonElement node) 
                || !node.TryGetInt32(out int width)
                || !element.TryGetProperty("y", out node)
                || !node.TryGetInt32(out int height)
                || !element.TryGetProperty("z", out node)
                || !node.TryGetInt32(out int length)
            )
                return false;
            value = new Vector(width, height, length);
            return true;
        }

        public void WriteDimension(Utf8JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WriteNumber("width", X);
            writer.WriteNumber("height", Y);
            writer.WriteNumber("length", Z);
            writer.WriteEndObject();
        }

        public void WritePosition(Utf8JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WriteNumber("x", X);
            writer.WriteNumber("y", Y);
            writer.WriteNumber("z", Z);
            writer.WriteEndObject();
        }

        public override bool Equals(object? obj)
        {
            return obj is Vector vector &&
                   X == vector.X &&
                   Y == vector.Y &&
                   Z == vector.Z;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y, Z);
        }

        public static Vector Zero { get; } = new Vector(0, 0, 0);

        public static Vector One { get; } = new Vector(1, 1, 1);

        public static Vector UnitX { get; } = new Vector(1, 0, 0);

        public static Vector UnitY { get; } = new Vector(0, 1, 0);

        public static Vector UnitZ { get; } = new Vector(0, 0, 1);

        public int CompareTo(Vector other)
        {
            Vector leftSortet = Sorted;
            Vector rightSortet = other.Sorted;
            var comp = leftSortet.X.CompareTo(rightSortet.X);
            if (comp == 0) {
                comp = leftSortet.Y.CompareTo(rightSortet.Y);
                if (comp == 0) {
                    comp = leftSortet.Y.CompareTo(rightSortet.Y);
                    if (comp == 0) {
                        return leftSortet.Z.CompareTo(rightSortet.Z);
                    } else {
                        return comp;
                    }
                } else {
                    return comp;
                }
            } else {
                return comp;
            }
        }

        public bool IsLowerThanBounds(Vector bounds)
        {
            return X < bounds.X || Y < bounds.Y || Z < bounds.Z;
        }

        public bool IsHigherThanBounds(Vector bounds)
        {
            return X > bounds.X || Y > bounds.Y || Z > bounds.Z;
        }

        public static Vector operator +(Vector left, Vector right)
        {
            return new Vector(left.X + right.X, left.Y + right.Y, left.Z + right.Z);
        }

        public static Vector operator -(Vector left, Vector right)
        {
            return new Vector(left.X - right.X, left.Y - right.Y, left.Z - right.Z);
        }

        public static Vector operator -(Vector vector)
        {
            return new Vector(-vector.X, -vector.Y, -vector.Z);
        }

        public static bool operator ==(Vector left, Vector right)
            => Equals(left, right);
        
        public static bool operator !=(Vector left, Vector right)
            => !Equals(left, right);
        
        public static Vector Min(Vector left, Vector right)
        {
            return new Vector(
                Math.Min(left.X, right.X), 
                Math.Min(left.Y, right.Y),
                Math.Min(left.Z, right.Z)
            );
        }

        public static Vector Max(Vector left, Vector right)
        {
            return new Vector(
                Math.Max(left.X, right.X), 
                Math.Max(left.Y, right.Y),
                Math.Max(left.Z, right.Z)
            );
        }

        public static Vector Abs(Vector value)
        {
            return new Vector(
                Math.Abs(value.X),
                Math.Abs(value.Y),
                Math.Abs(value.Z)
            );
        }

        public override string ToString()
        {
            return $"({X}, {Y}, {Z})";
        }

        public Vector Rotate(VectorRotation rotation)
        {
            return rotation switch
            {
                VectorRotation.XYZ => new Vector(X, Y, Z),
                VectorRotation.XZY => new Vector(X, Z, Y),
                VectorRotation.YXZ => new Vector(Y, X, Z),
                VectorRotation.YZX => new Vector(Y, Z, X),
                VectorRotation.ZXY => new Vector(Z, X, Y),
                VectorRotation.ZYX => new Vector(Z, Y, X),
                _ => throw new ArgumentException("invalid rotation", nameof(rotation)),
            };
        }

        public Vector RotateInverse(VectorRotation rotation)
        {
            return rotation switch
            {
                VectorRotation.XYZ => new Vector(X, Y, Z),
                VectorRotation.XZY => new Vector(X, Z, Y),
                VectorRotation.YXZ => new Vector(Y, X, Z),
                VectorRotation.YZX => new Vector(Z, X, Y),
                VectorRotation.ZXY => new Vector(Y, Z, X),
                VectorRotation.ZYX => new Vector(Z, Y, X),
                _ => throw new ArgumentException("invalid rotation", nameof(rotation)),
            };
        }

        public static ReadOnlyMemory<VectorRotation> AvailableRotations { get; } =
            new[]
            {
                VectorRotation.XYZ,
                VectorRotation.XZY,
                VectorRotation.YXZ,
                VectorRotation.YZX,
                VectorRotation.ZXY,
                VectorRotation.ZYX,
            };
    }
}