using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json;

namespace CompetetivePacking.Data
{
    public class NewGame
    {
        public Vector Container { get; }

        public Memory<Vector> Parcels { get; }

        public Memory<Vector> OpponentParcels { get; }

        public NewGame(Vector container, Memory<Vector> parcels, Memory<Vector> opponentParcels)
        {
            Container = container;
            Parcels = parcels;
            OpponentParcels = opponentParcels;
        }

        public static bool TryCreate(JsonElement json, [NotNullWhen(true)] out NewGame? value)
        {
            value = default;
            // contaner
            if (!json.TryGetProperty("container", out JsonElement node)
                || !Vector.TryCreateDimension(node, out Vector container)
            )
                return false;
            // parcels
            if (!json.TryGetProperty("parcels", out node) || node.ValueKind != JsonValueKind.Array)
                return false;
            var parcels = new List<Vector>();
            foreach (var entry in node.EnumerateArray())
                if (!Vector.TryCreateDimension(entry, out Vector dim))
                    return false;
                else parcels.Add(dim.Sorted);
            // opponent parcels
            if (!json.TryGetProperty("opponent_parcels", out node) || node.ValueKind != JsonValueKind.Array)
                return false;
            var opponentParcels = new List<Vector>();
            foreach (var entry in node.EnumerateArray())
                if (!Vector.TryCreateDimension(entry, out Vector dim))
                    return false;
                else opponentParcels.Add(dim.Sorted);
            // finish
            value = new NewGame(container, parcels.ToArray(), opponentParcels.ToArray());
            return true;
        }
    }
}