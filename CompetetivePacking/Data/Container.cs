using System;

namespace CompetetivePacking.Data
{
    public class Container
    {
        private readonly Memory<Axis> data; // Axis struct is the x-axis

        public Vector Size { get; }

        private int GetIndex(int y, int z)
        {
            return Size.Z * y + z;
        }

        public Container(Vector size)
        {
            if (size.X > 64)
                throw new ArgumentOutOfRangeException(nameof(size));
            if (size.IsLowerThanBounds(Vector.Zero))
                throw new ArgumentOutOfRangeException(nameof(size));
            Size = size;
            data = new Axis[size.Y * size.Z];
        }

        /// <summary>
        /// Checks in O(n^2) if a placement can be done
        /// </summary>
        /// <param name="job">the job with the placement of a block</param>
        /// <returns>true if no other placement intersects the selected job</returns>
        public bool CanPlace(Job job)
        {
            if (job.Position.IsLowerThanBounds(Vector.Zero) 
                || job.Parcel.IsLowerThanBounds(Vector.Zero)
                || (job.Position + job.Parcel).IsHigherThanBounds(Size)
            )
                return false;

            var span = data.Span;
            for (int y = 0; y < job.Parcel.Y; ++y)
            {
                for (int z = 0; z < job.Parcel.Z; ++z)
                {
                    var axis = span[GetIndex(y + job.Position.Y, z + job.Position.Z)];
                    if (!axis.CheckFreePlacement(job.Position.X, job.Parcel.X))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Place a job at the selected position. There is no check if the placement can be done. If
        /// there was already a block there it will just overwritten. <br/>
        /// This operation needs O(n^2).
        /// </summary>
        /// <param name="job">the selected placement</param>
        public void Place(Job job)
        {
            var span = data.Span;
            for (int y = 0; y < job.Parcel.Y; ++y)
            {
                for (int z = 0; z < job.Parcel.Z; ++z)
                {
                    var index = GetIndex(y + job.Position.Y, z + job.Position.Z);
                    var axis = span[index];
                    span[index] = axis.Place(job.Position.X, job.Parcel.X);
                }
            }
        }

        /// <summary>
        /// Copy the internal data to another <see cref="Container" />. This overwrites their data.
        /// This is only possible if the other container has the same size.
        /// </summary>
        /// <param name="other">the target container</param>
        public void CopyTo(Container other)
        {
            if (other.Size != Size)
                throw new InvalidOperationException("invalid container sizes");
            data.CopyTo(other.data);
        }

#if DEBUG
        public void DebugOutput()
        {
            for (int y = 0; y < Size.Y; ++y)
                for (int z = 0; z < Size.Z; ++z)
                    Console.Out.WriteLine($"(*,{y},{z}): {data.Span[GetIndex(y, z)]}");
        }
#endif
    }
}