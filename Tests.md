`new-game`

```json
{
    "container": { "width": 4, "height": 4, "length": 4 },
    "parcels": [
        { "width": 1, "height": 1, "length": 1 },
        { "width": 2, "height": 1, "length": 3 }
    ],
    "opponent_parcels": [
        { "width": 3, "height": 2, "length": 2 },
        { "width": 1, "height": 2, "length": 1 }
    ]
}
```

`opponent-parcel`

```json
{
    "parcel": { "width": 1, "height": 1, "length": 1 },
    "position": { "x": 0, "y": 0, "z": 0 }
}
```