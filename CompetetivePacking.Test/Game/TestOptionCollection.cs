using System;
using System.Threading.Tasks;
using CompetetivePacking.Data;
using CompetetivePacking.Game;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompetetivePacking.Test.Game
{
    [TestClass]
    public class TestOptionCollection
    {
        [TestMethod]
        public async Task TestAddingAndRemoving()
        {
            var container = new Container(new Vector(3, 3, 3));
            var col = new OptionCollection(container,
                new CompetetivePacking.Player.ParcelManagement(new []
                {
                    new Vector(1, 1, 1),
                })
            );
            var option = new Option(
                new Job(new Vector(1, 1, 1), new Vector(0, 0, 0)),
                0,
                new Vector(3, 3, 3),
                new Vector(1, 2, 3),
                0
            );
            Assert.IsNull(await col.GetBestOption());
            await col.AddOption(option);
            Assert.AreEqual(option, await col.GetBestOption());
            Assert.IsNull(await col.GetBestOption());
        }

        [TestMethod]
        public async Task TestSorting()
        {
            var container = new Container(new Vector(3, 3, 3));
            var col = new OptionCollection(container,
                new CompetetivePacking.Player.ParcelManagement(new []
                {
                    new Vector(1, 1, 1),
                    new Vector(1, 3, 1),
                })
            );
            var option1 = new Option(
                new Job(new Vector(1, 1, 1), new Vector(0, 0, 0)),
                0,
                new Vector(3, 3, 3),
                new Vector(1, 2, 3),
                0
            );
            var option2 = new Option(
                new Job(new Vector(1, 3, 1), new Vector(0, 0, 0)),
                1,
                new Vector(3, 3, 3),
                new Vector(1, 2, 3),
                -1
            );
            Assert.IsNull(await col.GetBestOption());
            // add in normal order
            await col.AddOption(option1);
            await col.AddOption(option2);
            Assert.AreEqual(option2, await col.GetBestOption());
            Assert.AreEqual(option1, await col.GetBestOption());
            Assert.IsNull(await col.GetBestOption());

            // add in reversed order
            await col.AddOption(option2);
            await col.AddOption(option1);
            Assert.AreEqual(option2, await col.GetBestOption());
            Assert.AreEqual(option1, await col.GetBestOption());
            Assert.IsNull(await col.GetBestOption());
        }

        [TestMethod]
        public async Task TestPruning()
        {
            var container = new Container(new Vector(3, 3, 3));
            var col = new OptionCollection(container,
                new CompetetivePacking.Player.ParcelManagement(new []
                {
                    new Vector(1, 1, 1),
                    new Vector(1, 3, 1)
                })
            );
            var option1 = new Option(
                new Job(new Vector(1, 1, 1), new Vector(0, 0, 0)),
                0,
                new Vector(3, 3, 3),
                new Vector(1, 2, 3),
                0
            );
            var option2 = new Option(
                new Job(new Vector(1, 3, 1), new Vector(0, 0, 0)),
                1,
                new Vector(3, 3, 3),
                new Vector(1, 2, 3),
                -1
            );
            Assert.IsNull(await col.GetBestOption());
            // add them
            await col.AddOption(option1);
            await col.AddOption(option2);
            Assert.AreEqual(2, col.Fill);

            // modify container
            container.Place(new Job(new Vector(1, 1, 1), new Vector(0, 2, 0)));
            container.DebugOutput();
            // cleanup the collection from invalid entries
            await col.Cleanup();
            Assert.AreEqual(1, col.Fill);
            // fetch results, option2 should be no longer available
            Assert.AreEqual(option1, await col.GetBestOption());
            Assert.IsNull(await col.GetBestOption());

            // test adding option2 will no longer add this
            await col.AddOption(option2);
            await col.AddOption(option1);
            Assert.AreEqual(1, col.Fill);
            Assert.AreEqual(option1, await col.GetBestOption());
            Assert.IsNull(await col.GetBestOption());
        }
    }
}