using System;
using System.Collections.Generic;
using System.Linq;
using CompetetivePacking.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CompetetivePacking.Player;

namespace CompetetivePacking.Test.Player
{
    [TestClass]
    public class TestHalbordnung
    {
        [TestMethod]
        public void TestInit()
        {
            var vectors = new[]
            {
                new Vector(2, 2, 2),
                new Vector(2, 3, 4),
                new Vector(1, 3, 2),
                new Vector(1, 5, 1),
            };
            var ho = new Halbordnung(vectors);
            var ids = vectors.Select(v => ho.getPositionOf(v)).ToArray();
            for (int i = 0; i < vectors.Length; ++i)
                Assert.AreEqual(vectors[i], ho.getParcelAt(ids[i]));
        }

        [TestMethod]
        public void TestContain()
        {
            var vectors = new[]
            {
                new Vector(2, 2, 2),
                new Vector(2, 3, 4),
                new Vector(1, 3, 2),
                new Vector(1, 5, 1),
            };
            var ho = new Halbordnung(vectors);
            var ids = vectors.Select(v => ho.getPositionOf(v)).ToArray();
            var idList = vectors.Select(v => ho.getPositionOf(v)).ToList();
            var valid = new HashSet<(int, int)>
            {
                (1, 0), (1, 2), (0, 0), (1, 1), (2, 2), (3, 3)
            };
            for (int i = 0; i < vectors.Length; ++i)
                for (int j = 0; j < vectors.Length; ++j)
                {
                    if (valid.Contains((i, j)))
                        Assert.IsTrue(
                            ho.canContain(ids[i], ids[j]), 
                            $"It was required that {vectors[i]} contains {vectors[j]}"
                        );
                    else
                        Assert.IsFalse(
                            ho.canContain(ids[i], ids[j]), 
                            $"It was required that {vectors[i]} doesn't contain {vectors[j]}"
                        );
                }
        }

        [TestMethod]
        public void TestLarger()
        {
            var vectors = new[]
            {
                new Vector(2, 2, 2),
                new Vector(2, 3, 4),
                new Vector(1, 3, 2),
                new Vector(1, 5, 1),
            };
            var ho = new Halbordnung(vectors);
            ho.computeFullRelation();
            var ids = vectors.Select(v => ho.getPositionOf(v)).ToArray();
            var valid = new Dictionary<int, List<int>>
            {
                { 0, new List<int> { ids[1] }},
                { 1, new List<int> {}},
                { 2, new List<int> { ids[1] }},
                { 3, new List<int> {}},
            };
            for (int i = 0; i < vectors.Length; ++i)
            {
                var expected = valid[i];
                var result = ho.getLarger(ids[i]).ToList();
                expected.Sort();
                result.Sort();
                Assert.AreEqual(expected.Count, result.Count, $"invalid number of return, i={i} {vectors[i]}");
                for (int j = 0; j < expected.Count; ++j)
                    Assert.AreEqual(expected[j], result[j]);
            }
        }

        [TestMethod]
        public void TestSmaller()
        {
            var vectors = new[]
            {
                new Vector(2, 2, 2),
                new Vector(2, 3, 4),
                new Vector(1, 3, 2),
                new Vector(1, 5, 1),
            };
            var ho = new Halbordnung(vectors);
            ho.computeFullRelation();
            var ids = vectors.Select(v => ho.getPositionOf(v)).ToArray();
            var valid = new Dictionary<int, List<int>>
            {
                { 0, new List<int> { }},
                { 1, new List<int> { ids[0], ids[2] }},
                { 2, new List<int> { }},
                { 3, new List<int> { }},
            };
            for (int i = 0; i < vectors.Length; ++i)
            {
                var expected = valid[i];
                var result = ho.getSmaller(ids[i]).ToList();
                expected.Sort();
                result.Sort();
                Assert.AreEqual(expected.Count, result.Count, "invalid number of return");
                for (int j = 0; j < expected.Count; ++j)
                    Assert.AreEqual(expected[j], result[j]);
            }
        }
    }
}