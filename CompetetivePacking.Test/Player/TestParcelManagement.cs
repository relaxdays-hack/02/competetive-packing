using System;
using CompetetivePacking.Player;
using CompetetivePacking.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompetetivePacking.Test.Player
{
    [TestClass]
    public class TestParcelManagement
    {
        [TestMethod]
        public void TestAnyResult()
        {
            Memory<Vector> data = new[]
            {
                new Vector(2, 3, 1),
                new Vector(4, 1, 2),
                new Vector(4, 1, 2),
            };
            var manager = new ParcelManagement(data);
            var result = manager.GetParcel(new Vector(4, 4, 4));
            Assert.IsNotNull(result);
            Assert.IsNull(manager.GetParcel(new Vector(1, 1, 1)));
        }

        [TestMethod]
        public void TestExistingResult()
        {
            Memory<Vector> data = new[]
            {
                new Vector(2, 3, 1),
            };
            var manager = new ParcelManagement(data);
            var result = manager.GetParcel(new Vector(4, 4, 4));
            Assert.IsNotNull(result);
            Assert.AreEqual(data.Span[0], result!.Value.parcel);
        }

        [TestMethod]
        public void TestMatchingResult()
        {
            Memory<Vector> data = new[]
            {
                new Vector(2, 3, 1),
                new Vector(4, 1, 2),
                new Vector(4, 1, 2),
            };
            var manager = new ParcelManagement(data);
            // only 2nd entry should match
            var result = manager.GetParcel(new Vector(4, 2, 4));
            Assert.IsNotNull(result);
            Assert.AreEqual(data.Span[1], result!.Value.parcel);
        }

        [TestMethod]
        public void TestUsedResult()
        {
            Memory<Vector> data = new[]
            {
                new Vector(2, 3, 1),
                new Vector(4, 1, 2),
            };
            var manager = new ParcelManagement(data);
            // take one
            var result = manager.GetParcel(new Vector(4, 4, 4));
            Assert.IsNotNull(result);
            manager.SetUsed(result!.Value.parcel);
            // take other
            var result2 = manager.GetParcel(new Vector(4, 4, 4));
            Assert.IsNotNull(result2);
            Assert.AreNotEqual(result, result2);
            manager.SetUsed(result2!.Value.parcel);
            // should be empty
            var result3 = manager.GetParcel(new Vector(4, 4, 4));
            Assert.IsNull(result3);
        }

        [TestMethod]
        public void TestMultiUseResult()
        {
            Memory<Vector> data = new[]
            {
                new Vector(4, 1, 2),
                new Vector(4, 1, 2),
            };
            var manager = new ParcelManagement(data);
            // first take
            var result = manager.GetParcel(new Vector(4, 2, 4));
            Assert.IsNotNull(result);
            Assert.AreEqual(data.Span[0], result!.Value.parcel);
            manager.SetUsed(result!.Value.parcel);
            // second take
            result = manager.GetParcel(new Vector(4, 4, 4));
            Assert.IsNotNull(result);
            Assert.AreEqual(data.Span[0], result!.Value.parcel);
            manager.SetUsed(result!.Value.parcel);
            // third take, should be empty
            result = manager.GetParcel(new Vector(4, 4, 4));
            Assert.IsNull(result);
        }
    }
}