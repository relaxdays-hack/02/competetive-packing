using CompetetivePacking.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text.Json;

namespace CompetetivePacking.Test.Data
{
    [TestClass]
    public class TestParsing
    {
        public void ParseJobTest()
        {
            var source = @"
{
    ""parcel"": { ""width"": 1, ""height"": 2, ""length"": 3 },
    ""position"": { ""x"": 4, ""y"": 5, ""z"": 6 }
}
            ";

            var doc = JsonDocument.Parse(source);
            Assert.IsTrue(Job.TryCreate(doc.RootElement, out Job? job));
            Assert.IsNotNull(job);
            Assert.AreEqual(1, job!.Value.Parcel.X);
            Assert.AreEqual(2, job!.Value.Parcel.Y);
            Assert.AreEqual(3, job!.Value.Parcel.Z);
            Assert.AreEqual(4, job!.Value.Position.X);
            Assert.AreEqual(5, job!.Value.Position.Y);
            Assert.AreEqual(6, job!.Value.Position.Z);
        }
    }
}