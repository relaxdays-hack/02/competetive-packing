using CompetetivePacking.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompetetivePacking.Test.Data
{
    [TestClass]
    public class TestVectorRotation
    {
        [TestMethod]
        public void TestRotation()
        {
            var vec = new Vector(1, 2, 3);
            foreach (var rotation in Vector.AvailableRotations.Span)
            {
                Assert.AreEqual(vec, vec.Rotate(rotation).RotateInverse(rotation));
            }
        }

    }
}