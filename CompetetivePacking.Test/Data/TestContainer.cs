using System;
using CompetetivePacking.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompetetivePacking.Test.Data
{
    [TestClass]
    public class TestContainer
    {
        [TestMethod]
        public void TestBlocking()
        {
            var container = new Container(new Vector(10, 10, 10));
            Assert.IsTrue(container.CanPlace(new Job(new Vector(5, 5, 5), new Vector(1, 2, 3))));
            Assert.IsFalse(container.CanPlace(new Job(new Vector(5, 5, 5), new Vector(6, 2, 3))));

            container.Place(new Job(new Vector(5, 5, 5), new Vector(2, 0, 3)));
            Assert.IsFalse(container.CanPlace(new Job(new Vector(5, 5, 5), new Vector(1, 2, 3))));
            Assert.IsTrue(container.CanPlace(new Job(new Vector(2, 2, 2), new Vector(0, 0, 0))));
        }
    }
}